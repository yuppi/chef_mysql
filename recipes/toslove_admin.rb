#
# Cookbook Name:: mysql
# Recipe:: toslove_admin
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#
#--------------------------------------------------------------------
# デフォルトレシピから変数を使用する
#--------------------------------------------------------------------
root_password = node['mysql']['default']['root_password']

#--------------------------------------------------------------------
# データベースの作成
#--------------------------------------------------------------------
db_name = node["mysql"]["toslove_admin"]["db_name"]
execute "create_db" do
    command "/usr/bin/mysql -u root -p#{root_password} < #{Chef::Config[:file_cache_path]}/toslove_admin_create_db.sql"
    action :nothing
    not_if "/usr/bin/mysql -u root -p#{root_password} -D #{db_name}"
end

template "#{Chef::Config[:file_cache_path]}/toslove_admin_create_db.sql" do
    owner "root"
    group "root"
    mode 0644
    source "toslove_admin_create_db.sql.erb"
    variables({
                  :db_name => db_name,
              })
    notifies :run, "execute[create_db]", :immediately
end

#--------------------------------------------------------------------
# ユーザーの作成
#--------------------------------------------------------------------
user_name     = node["mysql"]["toslove_admin"]["user"]["name"]
user_password = node["mysql"]["toslove_admin"]["user"]["password"]
execute "create_user" do
    command "/usr/bin/mysql -u root -p#{root_password} < #{Chef::Config[:file_cache_path]}/toslove_admin_create_user.sql"
    action :nothing
    not_if "/usr/bin/mysql -u #{user_name} -p#{user_password} -D #{db_name}"
end

template "#{Chef::Config[:file_cache_path]}/toslove_admin_create_user.sql" do
    owner "root"
    group "root"
    mode 0644
    source "toslove_admin_create_user.sql.erb"
    variables({
                  :db_name => db_name,
                  :username => user_name,
                  :password => user_password,
              })
    notifies :run, "execute[create_user]", :immediately
end

# clientでの接続用ファイル作る
# "Using a password on the command line interface can be insecure"対策
template "/etc/mysql-user-toslove-admin.cnf" do
  owner "root"
  group "root"
  mode 0644
  source "mysql-user-toslove-admin.cnf.erb"
  variables({
                :db_name => db_name,
                :username => user_name,
                :password => user_password,
            })
  not_if "ls /etc/mysql-user-toslove-admin.cnf"
end

#--------------------------------------------------------------------
# 初期テーブル作成
#--------------------------------------------------------------------
execute "toslove_admin-create-initial-tables" do
  command "/usr/bin/mysql --defaults-extra-file=/etc/mysql-user-toslove-admin.cnf -D #{db_name} < #{Chef::Config[:file_cache_path]}/toslove_admin_initial.sql"
  action :nothing
  not_if "/usr/bin/mysql --defaults-extra-file=/etc/mysql-user-toslove-admin.cnf -D #{db_name} -e 'show tables' | grep 'admin_users'"
end

cookbook_file "#{Chef::Config[:file_cache_path]}/toslove_admin_initial.sql" do
  owner "root"
  group "root"
  mode 0644
  notifies :run, "execute[toslove_admin-create-initial-tables]", :immediately
end