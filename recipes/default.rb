#
# Cookbook Name:: mysql
# Recipe:: default
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#

#--------------------------------------------------------------------
# 設定情報
#--------------------------------------------------------------------
# MySQL公式リポジトリを置くディレクトリを作成
directory node['mysql']['default']['work_dir'] do
    action :create
    recursive true
    not_if "ls -d #{node['mysql']['default']['work_dir']}"
end
directory node['mysql']['default']['log_dir'] do
    action :create
    owner "root"
    group "root"
    recursive true
    not_if "ls -d #{node['mysql']['default']['log_dir']}"
end


# MySQL公式リポジトリ情報をDL＠RHEL6
remote_file node['mysql']['default']['work_dir'] + node['mysql']['default']['source_file_name'] do
    source node['mysql']['default']['repository_url'] + node['mysql']['default']['source_file_name']
    not_if "ls #{node['mysql']['default']['work_dir']}#{node['mysql']['default']['source_file_name']}"
end

#--------------------------------------------------------------------
# MySQL公式のリポジトリを追加する
#--------------------------------------------------------------------
# RPMをインストール
rpm_package "mysql-server-repos-rpm" do
    action :install
    source node['mysql']['default']['work_dir'] + node['mysql']['default']['source_file_name']
end

yum_package "mysql-community-server" do
    action :install
    flush_cache [:before]
end

#--------------------------------------------------------------------
# my.cnfの設定
#--------------------------------------------------------------------
template "/etc/my.cnf" do
    path "/etc/my.cnf"
    source "my.cnf.erb"
    mode 0644
end


# MySQLサービスの設定
service "mysqld" do
    action [ :enable, :start ]
    # action [ :enable ]
    supports :status => true, :restart => true, :reload => true
    # subscribes :restart, resources(:template => "/etc/my.cnf")
end


#--------------------------------------------------------------------
# rootユーザーの設定
#--------------------------------------------------------------------
root_password = node["mysql"]['default']["root_password"]
execute "secure_install" do
    command "/usr/bin/mysql -u root < #{Chef::Config[:file_cache_path]}/secure_install.sql"
    action :nothing
    only_if "/usr/bin/mysql -u root -e 'show databases;'"
end

template "#{Chef::Config[:file_cache_path]}/secure_install.sql" do
    owner "root"
    group "root"
    mode 0644
    source "secure_install.sql.erb"
    variables({
                  :root_password => root_password,
              })
    notifies :run, "execute[secure_install]", :immediately
end

