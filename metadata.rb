name             'mysql'
maintainer       'TOSLOVE.com'
maintainer_email 'Yuki Suzuki'
license          'All rights reserved'
description      'Installs/Configures mysql'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.1'
