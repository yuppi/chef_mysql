# ************************************************************
# Sequel Pro SQL dump
# バージョン 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# ホスト: toslove.dev (MySQL 5.6.22-log)
# データベース: toslove
# 作成時刻: 2015-01-25 07:39:41 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# テーブルのダンプ article_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `article_images`;

CREATE TABLE `article_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) unsigned NOT NULL,
  `image_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`),
  KEY `image_id` (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ article_movies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `article_movies`;

CREATE TABLE `article_movies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`),
  KEY `movie_id` (`movie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ article_relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `article_relations`;

CREATE TABLE `article_relations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_articles_id` int(10) unsigned NOT NULL,
  `child_articles_id` int(10) unsigned NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_articles_id` (`parent_articles_id`),
  KEY `child_articles_id` (`child_articles_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ article_tag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `article_tag`;

CREATE TABLE `article_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `article_tag` WRITE;
/*!40000 ALTER TABLE `article_tag` DISABLE KEYS */;

INSERT INTO `article_tag` (`id`, `article_id`, `tag_id`, `created_at`, `updated_at`)
VALUES
	(10,1,9,'2014-10-07 15:11:14','2014-10-07 15:11:14'),
	(11,1,10,'2014-10-07 15:11:14','2014-10-07 15:11:14'),
	(13,1,1,'2014-10-07 15:52:11','2014-10-07 15:52:11');

/*!40000 ALTER TABLE `article_tag` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL COMMENT '記事種別（article, story）',
  `title` varchar(255) DEFAULT '' COMMENT '記事タイトル',
  `body_md` longtext COMMENT '記事本文（Markdown形式）',
  `body` longtext COMMENT '記事本文（HTML形式）',
  `meta_title` varchar(32) DEFAULT NULL COMMENT 'ページメタデータ（タイトル）',
  `meta_keyword` varchar(128) DEFAULT NULL COMMENT 'ページメタデータ（キーワード）',
  `meta_description` varchar(128) DEFAULT NULL COMMENT 'ページメタデータ（ディスクリプション）',
  `tumbnail_filename` varchar(64) DEFAULT NULL COMMENT '記事トップサムネイル画像ファイル名',
  `published_from` datetime DEFAULT NULL COMMENT '公開日時（開始日時）',
  `published_to` datetime DEFAULT NULL COMMENT '公開日時（終了日時）',
  `deleted_at` datetime DEFAULT NULL COMMENT '削除日時',
  `created_at` datetime NOT NULL COMMENT '作成日時',
  `updated_at` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;

INSERT INTO `articles` (`id`, `type`, `title`, `body_md`, `body`, `meta_title`, `meta_keyword`, `meta_description`, `tumbnail_filename`, `published_from`, `published_to`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,'article','QUESTIONS AND ANSWERS ABOUT TOS #13','# 概要\r\n既に構築済みのEC2インスタンスに新しくEBSボリュームを追加してEC2で使用できるようにします。\r\n\r\naaa\r\nalert(\"ok\");\r\n\r\n\r\n> **EBSとは**\r\n>\r\n> Elastic Block Storeの略\r\n> EC2への仮想外付けストレージサービス\r\n> ・サイズは1GB単位で～1TBまで\r\n> ・サイズ/期間/IOで課金される\r\n>\r\n> 詳しくは下記を参照してください\r\n> [[AWSマイスターシリーズ] Elastic Block Store(EBS)](http://www.slideshare.net/AmazonWebServicesJapan/aws-16148274)\r\n\r\n## 作業イメージ図\r\n\r\n![名称未設定-1.png](https://qiita-image-store.s3.amazonaws.com/0/34281/e72dcc48-b5dc-3ae5-8c31-d2b9ca2658ec.png \"名称未設定-1.png\")\r\n\r\n# 作業手順\r\n## EBSボリュームを作成する\r\n\r\n1.AWS ManagementConsoleの「Services」から「EC2」を選択します。\r\n\r\n2.左側のナビゲーションから「ELASTIC BLOCK STORE」の「Volumes」を開いて「Create Volumes」をクリックして作成ダイアログを開きます。\r\n\r\n![EC2_Management_Console.png](https://qiita-image-store.s3.amazonaws.com/0/34281/fc485559-ab01-0d56-4596-35ecfb9bfc6c.png \"EC2_Management_Console.png\")\r\n\r\n3.必要な項目を入力して、「Create」をクリックしてEBSを作成する\r\n\r\n![EC2_Management_Console.png](https://qiita-image-store.s3.amazonaws.com/0/34281/beed3893-87b3-9f52-42d1-49f49d6dc030.png \"EC2_Management_Console.png\")\r\n\r\n\r\n| 項目名 | 説明 |\r\n|:-----------|:------------|\r\n| VolumeType|通常は「standard」を選択。I/O性能を設定したい場合は「Provisioned IOPS」を選択。|\r\n|Size|ボリュームサイズ（ストレージサイズ）を入力します。|\r\n|IOPS|「Provisioned IOPS」を選択した場合のI/O性能を設定します。|\r\n|Availability Zone|EBSを作成するゾーンを設定します。``注意：ゾーンは追加したいEC2インスタンスと同じゾーンを指定しましょう``|\r\n|Snapshot|EBS作成時に元になるSnapshotを指定する場合に使用します。Snapshotの内容を使いたいけど、Snapshotとは異なるボリュームサイズにしたい時等に使用します。|\r\n\r\n4.作成したEBSが一覧に追加され、「State」が「available」になったらEBS作成完了です。\r\n![EC2_Management_Console.png](https://qiita-image-store.s3.amazonaws.com/0/34281/dd4628d4-7ccd-62be-c837-84e860bbe5f6.png \"EC2_Management_Console.png\")\r\n\r\n## 既存のEC2に作成したEBSを追加する\r\n\r\n1.EBS一覧から追加したいEBSにチェックを入れて、「Actions」メニューまたは右クリックメニューで「Attach Volume」を選択します\r\n\r\n![EC2_Management_Console.png](https://qiita-image-store.s3.amazonaws.com/0/34281/ed9c71fb-2021-efc4-06a9-49dfa8df3e9d.png \"EC2_Management_Console.png\")\r\n\r\n2.必要な項目を入力して、「Attach」をクリックしてEC2にEBSを追加します。\r\n![EC2_Management_Console.png](https://qiita-image-store.s3.amazonaws.com/0/34281/915e0657-735e-31eb-23e8-4a679049146c.png \"EC2_Management_Console.png\")\r\n\r\n| 項目名 | 説明 |\r\n|:-----------|:------------|\r\n| VolumeType|選択したEBSボリューム|\r\n|Instance|EBSを追加したいEC2インスタンスを指定する。追加可能なEC2リストから選択するだけです。|\r\n|Device|EC2内でのデバイス名を指定します。通常はInstanceを選択した時点で最適なデバイス名が自動で設定されます。手動で設定ももちろん可能です。|\r\n\r\n3.EBS一覧で正常に追加できたか確認する。\r\n\r\n![EC2_Management_Console.png](https://qiita-image-store.s3.amazonaws.com/0/34281/02acc3ba-a0cb-7654-a958-4fb8cee61ddd.png \"EC2_Management_Console.png\")\r\n\r\n## EBSボリュームをEC2にマウントする\r\n追加したEBSをEC2で使用できるようにマウントします。\r\n\r\n1.EC2へSSH接続する。\r\n``\r\n操作するEC2のOSはUbuntuですが、基本的にAmazon Linuxでも同様の操作になります。\r\nubuntuだと/dev/sdgは/dev/xvdgになっている。\r\n``\r\n\r\n2.現在のマウント状態を確認する（``/dev/sdg``はまだマウントされていない）\r\n\r\n~~~\r\n$ df -h\r\nFilesystem      Size  Used Avail Use% Mounted on\r\n/dev/xvda1       20G  8.0G   11G  43% /\r\nnone            4.0K     0  4.0K   0% /sys/fs/cgroup\r\nudev            816M  8.0K  816M   1% /dev\r\ntmpfs           166M  200K  166M   1% /run\r\nnone            5.0M     0  5.0M   0% /run/lock\r\nnone            827M     0  827M   0% /run/shm\r\nnone            100M     0  100M   0% /run/user\r\n/dev/xvdf        20G  2.0G   17G  11% /data\r\n~~~\r\n3.EBSボリュームをフォーマットし、``/data2``にマウントします。\r\n\r\n```\r\n$ sudo su -\r\n\r\n# mkfs -t ext4 /dev/xvdg\r\nmke2fs 1.42.5 (29-Jul-2012)\r\nFilesystem label=\r\n・\r\n・\r\n・\r\nCreating journal (32768 blocks): done\r\nWriting superblocks and filesystem accounting information: done\r\n\r\n# mkdir /data2\r\n# mount /dev/xvdg /data2\r\n# exit\r\n```\r\n\r\n4.正常にマウントできているか確認する\r\n\r\n```\r\n\r\n$ df -h\r\nFilesystem      Size  Used Avail Use% Mounted on\r\n/dev/xvda1       20G  8.0G   11G  43% /\r\nnone            4.0K     0  4.0K   0% /sys/fs/cgroup\r\nudev            816M  8.0K  816M   1% /dev\r\ntmpfs           166M  200K  166M   1% /run\r\nnone            5.0M     0  5.0M   0% /run/lock\r\nnone            827M     0  827M   0% /run/shm\r\nnone            100M     0  100M   0% /run/user\r\n/dev/xvdf        20G  2.0G   17G  11% /data\r\n/dev/xvdg        99G   60M   94G   1% /data2\r\n\r\n```\r\n5.書き込みが正常にできるか確認する\r\n\r\n```\r\n\r\n# mkdir -p /data2/test/file/to/\r\n# echo \"this is text file\" > /data2/test/file/to/test.txt\r\n# cat /data2/test/file/to/test.txt\r\nthis is text file\r\n\r\n```\r\n## 自動マウントを設定する\r\nこのままだとEC2インスタンスをSTOP等をした際に、マウントが外れてしまうので、自動マウントするように設定しておく。\r\n意外と忘れがち。\r\n以下を末尾に追加する。\r\n\r\n1.自動マウントの設定をする\r\n\r\n```/etc/fstab\r\n/dev/xvdg /data2 ext4 defaults 1 1\r\n```\r\n\r\n2.（可能なら）自動マウントできているか確認する\r\nEC2をAWS ConsoleからStop -> Startをして自動マウントできているか確認する','<h1>概要</h1>\n<p>既に構築済みのEC2インスタンスに新しくEBSボリュームを追加してEC2で使用できるようにします。</p>\n<p>aaa\nalert(&quot;ok&quot;);</p>\n<blockquote>\n<p><strong>EBSとは</strong></p>\n<p>Elastic Block Storeの略\nEC2への仮想外付けストレージサービス\n・サイズは1GB単位で～1TBまで\n・サイズ/期間/IOで課金される</p>\n<p>詳しくは下記を参照してください\n<a href=\"http://www.slideshare.net/AmazonWebServicesJapan/aws-16148274\">[AWSマイスターシリーズ] Elastic Block Store(EBS)</a></p>\n</blockquote>\n<h2>作業イメージ図</h2>\n<p><img src=\"https://qiita-image-store.s3.amazonaws.com/0/34281/e72dcc48-b5dc-3ae5-8c31-d2b9ca2658ec.png\" alt=\"名称未設定-1.png\" title=\"名称未設定-1.png\" /></p>\n<h1>作業手順</h1>\n<h2>EBSボリュームを作成する</h2>\n<p>1.AWS ManagementConsoleの「Services」から「EC2」を選択します。</p>\n<p>2.左側のナビゲーションから「ELASTIC BLOCK STORE」の「Volumes」を開いて「Create Volumes」をクリックして作成ダイアログを開きます。</p>\n<p><img src=\"https://qiita-image-store.s3.amazonaws.com/0/34281/fc485559-ab01-0d56-4596-35ecfb9bfc6c.png\" alt=\"EC2_Management_Console.png\" title=\"EC2_Management_Console.png\" /></p>\n<p>3.必要な項目を入力して、「Create」をクリックしてEBSを作成する</p>\n<p><img src=\"https://qiita-image-store.s3.amazonaws.com/0/34281/beed3893-87b3-9f52-42d1-49f49d6dc030.png\" alt=\"EC2_Management_Console.png\" title=\"EC2_Management_Console.png\" /></p>\n<table>\n<thead>\n<tr>\n<th style=\"text-align: left;\">項目名</th>\n<th style=\"text-align: left;\">説明</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td style=\"text-align: left;\">VolumeType</td>\n<td style=\"text-align: left;\">通常は「standard」を選択。I/O性能を設定したい場合は「Provisioned IOPS」を選択。</td>\n</tr>\n<tr>\n<td style=\"text-align: left;\">Size</td>\n<td style=\"text-align: left;\">ボリュームサイズ（ストレージサイズ）を入力します。</td>\n</tr>\n<tr>\n<td style=\"text-align: left;\">IOPS</td>\n<td style=\"text-align: left;\">「Provisioned IOPS」を選択した場合のI/O性能を設定します。</td>\n</tr>\n<tr>\n<td style=\"text-align: left;\">Availability Zone</td>\n<td style=\"text-align: left;\">EBSを作成するゾーンを設定します。<code>注意：ゾーンは追加したいEC2インスタンスと同じゾーンを指定しましょう</code></td>\n</tr>\n<tr>\n<td style=\"text-align: left;\">Snapshot</td>\n<td style=\"text-align: left;\">EBS作成時に元になるSnapshotを指定する場合に使用します。Snapshotの内容を使いたいけど、Snapshotとは異なるボリュームサイズにしたい時等に使用します。</td>\n</tr>\n</tbody>\n</table>\n<p>4.作成したEBSが一覧に追加され、「State」が「available」になったらEBS作成完了です。\n<img src=\"https://qiita-image-store.s3.amazonaws.com/0/34281/dd4628d4-7ccd-62be-c837-84e860bbe5f6.png\" alt=\"EC2_Management_Console.png\" title=\"EC2_Management_Console.png\" /></p>\n<h2>既存のEC2に作成したEBSを追加する</h2>\n<p>1.EBS一覧から追加したいEBSにチェックを入れて、「Actions」メニューまたは右クリックメニューで「Attach Volume」を選択します</p>\n<p><img src=\"https://qiita-image-store.s3.amazonaws.com/0/34281/ed9c71fb-2021-efc4-06a9-49dfa8df3e9d.png\" alt=\"EC2_Management_Console.png\" title=\"EC2_Management_Console.png\" /></p>\n<p>2.必要な項目を入力して、「Attach」をクリックしてEC2にEBSを追加します。\n<img src=\"https://qiita-image-store.s3.amazonaws.com/0/34281/915e0657-735e-31eb-23e8-4a679049146c.png\" alt=\"EC2_Management_Console.png\" title=\"EC2_Management_Console.png\" /></p>\n<table>\n<thead>\n<tr>\n<th style=\"text-align: left;\">項目名</th>\n<th style=\"text-align: left;\">説明</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td style=\"text-align: left;\">VolumeType</td>\n<td style=\"text-align: left;\">選択したEBSボリューム</td>\n</tr>\n<tr>\n<td style=\"text-align: left;\">Instance</td>\n<td style=\"text-align: left;\">EBSを追加したいEC2インスタンスを指定する。追加可能なEC2リストから選択するだけです。</td>\n</tr>\n<tr>\n<td style=\"text-align: left;\">Device</td>\n<td style=\"text-align: left;\">EC2内でのデバイス名を指定します。通常はInstanceを選択した時点で最適なデバイス名が自動で設定されます。手動で設定ももちろん可能です。</td>\n</tr>\n</tbody>\n</table>\n<p>3.EBS一覧で正常に追加できたか確認する。</p>\n<p><img src=\"https://qiita-image-store.s3.amazonaws.com/0/34281/02acc3ba-a0cb-7654-a958-4fb8cee61ddd.png\" alt=\"EC2_Management_Console.png\" title=\"EC2_Management_Console.png\" /></p>\n<h2>EBSボリュームをEC2にマウントする</h2>\n<p>追加したEBSをEC2で使用できるようにマウントします。</p>\n<p>1.EC2へSSH接続する。\n<code> 操作するEC2のOSはUbuntuですが、基本的にAmazon Linuxでも同様の操作になります。 ubuntuだと/dev/sdgは/dev/xvdgになっている。 </code></p>\n<p>2.現在のマウント状態を確認する（<code>/dev/sdg</code>はまだマウントされていない）</p>\n<pre><code>$ df -h\nFilesystem      Size  Used Avail Use% Mounted on\n/dev/xvda1       20G  8.0G   11G  43% /\nnone            4.0K     0  4.0K   0% /sys/fs/cgroup\nudev            816M  8.0K  816M   1% /dev\ntmpfs           166M  200K  166M   1% /run\nnone            5.0M     0  5.0M   0% /run/lock\nnone            827M     0  827M   0% /run/shm\nnone            100M     0  100M   0% /run/user\n/dev/xvdf        20G  2.0G   17G  11% /data</code></pre>\n<p>3.EBSボリュームをフォーマットし、<code>/data2</code>にマウントします。</p>\n<pre><code>$ sudo su -\n\n# mkfs -t ext4 /dev/xvdg\nmke2fs 1.42.5 (29-Jul-2012)\nFilesystem label=\n・\n・\n・\nCreating journal (32768 blocks): done\nWriting superblocks and filesystem accounting information: done\n\n# mkdir /data2\n# mount /dev/xvdg /data2\n# exit</code></pre>\n<p>4.正常にマウントできているか確認する</p>\n<pre><code>\n$ df -h\nFilesystem      Size  Used Avail Use% Mounted on\n/dev/xvda1       20G  8.0G   11G  43% /\nnone            4.0K     0  4.0K   0% /sys/fs/cgroup\nudev            816M  8.0K  816M   1% /dev\ntmpfs           166M  200K  166M   1% /run\nnone            5.0M     0  5.0M   0% /run/lock\nnone            827M     0  827M   0% /run/shm\nnone            100M     0  100M   0% /run/user\n/dev/xvdf        20G  2.0G   17G  11% /data\n/dev/xvdg        99G   60M   94G   1% /data2\n</code></pre>\n<p>5.書き込みが正常にできるか確認する</p>\n<pre><code>\n# mkdir -p /data2/test/file/to/\n# echo \"this is text file\" &gt; /data2/test/file/to/test.txt\n# cat /data2/test/file/to/test.txt\nthis is text file\n</code></pre>\n<h2>自動マウントを設定する</h2>\n<p>このままだとEC2インスタンスをSTOP等をした際に、マウントが外れてしまうので、自動マウントするように設定しておく。\n意外と忘れがち。\n以下を末尾に追加する。</p>\n<p>1.自動マウントの設定をする</p>\n<p>```/etc/fstab\n/dev/xvdg /data2 ext4 defaults 1 1</p>\n<pre><code>\n\n2.（可能なら）自動マウントできているか確認する\nEC2をAWS ConsoleからStop -&gt; Startをして自動マウントできているか確認する</code></pre>','aaaaa','','','e72dcc48-b5dc-3ae5-8c31-d2b9ca2658ec.png','2014-08-23 15:07:58',NULL,NULL,'2014-09-21 09:08:12','2015-01-25 14:55:14'),
	(2,'article','test',NULL,'aaaa',NULL,NULL,NULL,NULL,'2014-09-25 00:00:06',NULL,NULL,'2014-09-25 00:00:09','2014-09-25 00:00:11'),
	(3,'article','test2',NULL,'bbb',NULL,NULL,NULL,NULL,'2014-09-25 00:00:48',NULL,NULL,'2014-09-25 00:00:50','2014-09-25 00:00:51');

/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ code_masters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `code_masters`;

CREATE TABLE `code_masters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(32) DEFAULT NULL COMMENT 'カテゴリ',
  `key` varchar(32) DEFAULT NULL COMMENT 'キー',
  `value` varchar(64) DEFAULT NULL COMMENT '値',
  `created_at` datetime NOT NULL COMMENT '作成日時',
  `updated_at` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ image_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `image_tags`;

CREATE TABLE `image_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL,
  `image_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tag_id` (`tag_id`),
  KEY `image_id` (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ステータス（0 = 未処理, 1=処理中, 2=処理済）',
  `user_id` int(11) DEFAULT NULL COMMENT 'ユーザーID',
  `title` varchar(255) DEFAULT '' COMMENT 'タイトル',
  `caption` text COMMENT 'キャプション',
  `filename` varchar(64) DEFAULT NULL COMMENT 'ファイル名',
  `original_filename` varchar(255) DEFAULT '' COMMENT 'オリジナルファイル名',
  `extension` varchar(16) NOT NULL DEFAULT '' COMMENT '拡張子',
  `width` float DEFAULT '0' COMMENT '幅',
  `height` float DEFAULT '0' COMMENT '高さ',
  `deleted_at` datetime DEFAULT NULL COMMENT '削除日時',
  `created_at` datetime NOT NULL COMMENT '作成日時',
  `updated_at` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ movie_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `movie_tags`;

CREATE TABLE `movie_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `movie_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `movie_id` (`movie_id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ movies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `movies`;

CREATE TABLE `movies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ステータス（0 = 未処理, 1=処理中, 2=処理済）',
  `user_id` int(11) DEFAULT NULL COMMENT 'ユーザーID',
  `title` varchar(255) DEFAULT '' COMMENT 'タイトル',
  `caption` text COMMENT 'キャプション',
  `filename` varchar(64) DEFAULT NULL COMMENT 'ファイル名',
  `original_filename` varchar(255) DEFAULT '' COMMENT 'オリジナルファイル名',
  `extension` varchar(16) NOT NULL COMMENT '拡張子',
  `duration` float DEFAULT '0' COMMENT '時間',
  `deleted_at` datetime DEFAULT NULL COMMENT '削除日時',
  `created_at` datetime NOT NULL COMMENT '作成日時',
  `updated_at` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# テーブルのダンプ tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT '',
  `tree` varchar(255) DEFAULT NULL,
  `key` varchar(64) DEFAULT '',
  `name` varchar(128) DEFAULT '',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;

INSERT INTO `tags` (`id`, `type`, `tree`, `key`, `name`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,'category','.1.','update','アップデート',NULL,'2014-10-07 13:05:59','2014-10-07 13:06:01'),
	(2,'category','.2.','event','イベント',NULL,'2014-09-21 13:16:31','2014-09-21 13:16:32'),
	(3,'category','.3.','maintenance','メンテナンス',NULL,'2014-09-21 13:17:02','2014-09-21 13:17:03'),
	(4,'category','.4.','official_korea','公式韓国',NULL,'2014-09-21 13:18:02','2014-09-21 13:18:03'),
	(5,'category','.5.','official_blog','公式ブログ',NULL,'2014-10-07 13:46:58','2014-10-07 13:46:59'),
	(6,'category','.6.','feature','特集',NULL,'2014-09-21 13:23:21','2014-09-21 13:23:22'),
	(7,'category','.7.','other_newssite','外部ニュースサイト',NULL,'2014-09-21 13:44:50','2014-09-21 13:44:51'),
	(8,'category','.7.8.','4gamer','4Gamer',NULL,'2014-09-21 13:46:21','2014-09-21 13:46:22'),
	(9,'keyword',NULL,NULL,'職業',NULL,'2014-10-07 13:45:45','2014-10-07 13:45:46'),
	(10,'keyword',NULL,NULL,'スキル',NULL,'2014-10-07 13:46:33','2014-10-07 13:46:34'),
	(13,'keyword',NULL,'','新規タグ',NULL,'2014-10-07 15:10:56','2014-10-07 15:10:56');

/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;


# テーブルのダンプ users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL DEFAULT 'member',
  `name` varchar(64) DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `remember_token` varchar(255) NOT NULL DEFAULT '',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `type`, `name`, `email`, `password`, `remember_token`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,'admin','system','yuppi0412@gmail.com','ty76mn58','',NULL,'2014-09-22 10:37:42','2014-09-22 10:37:43');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
