#
# レシピで使用する変数を設定する
#
# 作業用ディレクトリ
default['mysql']['default']['work_dir']  = '/usr/local/src/mysql/'
default['mysql']['default']['data_dir']  = '/var/lib/mysql'
default['mysql']['default']['log_dir']   = '/var/log/mysql/'

# ソースコードの URL
default['mysql']['default']['repository_url']      = 'http://dev.mysql.com/get/'
default['mysql']['default']['source_file_name']    = "mysql-community-release-el6-5.noarch.rpm"

# rootユーザー設定
default['mysql']['default']['root_password']       = 'abcd1234'